/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/
#include "assign2.h"
#include "projectile.h"
projectilePtr * projectileArray;
int numProjectiles;

/*Initiate a new projectile struct and initiate values*/
void createProjectile(){
	int i;
	projectilePtr p;
	if(numProjectiles>MAX_PROJECTILES){
		return;
	}
	p=(projectilePtr)calloc(1,sizeof(struct projectile));
	if(p==NULL){printf("ERROR: Memory allocation error"); return;}
		/*If fired from red base xPos is -1*/
		if(global.redProjectile==TRUE)
		{
		p->rotateAngle=global.redGunAngle; 
		p->posX=-1+PROJECTILE_R;
		p->redProjectile=FALSE;
		}
		/*If fired from green base xPos is 1*/
		else
		{
		p->rotateAngle=M_PI-global.greenGunAngle;
		p->posX=1;
		p->redProjectile=TRUE;
		}	
	/*Initiate variables*/
	p->vecX=cosf(p->rotateAngle);
	p->vecY=sinf(p->rotateAngle);
	p->vecZ=0;
	p->posY=0;
	p->posZ=0;
	p->accel=1;
	numProjectiles++;
	/*place in array in first free slot*/
	for(i=0;i<=numProjectiles;i++){
		if(projectileArray[i]==NULL){
			projectileArray[i]=p;
			return;
		}
	}
}

/*Allocate memory to store MAX_PROJECTILE worth of projectile structs*/
int initialiseProjectileArray(){
	numProjectiles=0;
	projectileArray=calloc(MAX_PROJECTILES,sizeof(struct projectile));
	if(projectileArray==NULL){
	printf("ERROR: Memory allocation error");
	return 1;
	}else{ return 0;}
	
}
/*Draws all projectiles stored in projectileArray*/
void drawAllProjectiles(){
	int i;
	projectilePtr p;
		updateProjectiles();
	for(i=0;i<MAX_PROJECTILES;i++)
	{
	p=projectileArray[i];
	if(p!=NULL){
	glMatrixMode(GL_MODELVIEW);
		if(global.redProjectile==TRUE)
		{
		glTranslatef(p->posX,p->posY,p->posZ);
		glutSolidSphere(PROJECTILE_R,16,16);
		glTranslatef(-p->posX,-p->posY,-p->posZ);
		}else
		{
		glTranslatef(p->posX,p->posY,p->posZ);
		glutSolidSphere(PROJECTILE_R,16,16);
		glTranslatef(-p->posX,-p->posY,-p->posZ);
		}
	}
	}	
}

/*Update positions and vectors of all projectiles in projectileArray,
 destroy projectile if collision occurs*/
void updateProjectiles(){
	int i,j;
	projectilePtr p;
	float distFromCntr;
	
	for(i=0;i<MAX_PROJECTILES;i++){
	p=projectileArray[i];
		if(p!=NULL)
		{
			p->vecX-=global.difTime*G_CONSTANT*p->posX;
			p->vecY-=global.difTime*G_CONSTANT*p->posY;
			p->posX+=global.difTime*p->accel*p->vecX;
			p->posY+=global.difTime*p->accel*p->vecY;
			p->posZ+=global.difTime*p->accel*p->vecZ;
			distFromCntr=sqrt(p->posX*p->posX+p->posY*p->posY); /*a^2+b^2=c^2*/
			/*Check if collision occured*/
			if(distFromCntr>1||checkShadeVertexCollision(p)||distFromCntr<0.07+PROJECTILE_R
				||checkBaseCollision(p))
			{
			projectileArray[i]=NULL;
			numProjectiles--;
			free(p);
			}
		}
	}
}

/*Checks if collision with shades occurs*/
int checkShadeVertexCollision(projectilePtr p){
	int i,j,faces=global.faces;
	float x,y,newangle;
	for(j=0;j<4;j++)
	{
		for(i=2*j*(faces-(faces/4));i<=(2*j+1)*(faces-(faces/4));i+=6)
		{
			x=vertices.ringVertices[i]*0.4;
			y=vertices.ringVertices[i+1]*0.4;
			newangle=atanf(x/y);
			if(y<0){
			x=-cosf(newangle+global.spin/(180/(2*M_PI))+M_PI/4)*0.4;
			y=-sinf(newangle+global.spin/(180/(2*M_PI))+M_PI/4)*0.4;
			}else{
			x=cosf(newangle+global.spin/(180/(2*M_PI))+M_PI/4)*0.4;
			y=sinf(newangle+global.spin/(180/(2*M_PI))+M_PI/4)*0.4;
			}
			
			if(p->posX-x<=PROJECTILE_R+0.04&&p->posX-x>=0
				&&p->posY-y<=PROJECTILE_R+0.04&&p->posY-y>=0){
			return TRUE;
			}
		}
	}
	return FALSE;
}

int checkBaseCollision(projectilePtr p){
	float baseX,baseY,fudge;
	if(p->redProjectile==FALSE)
		{baseX=1;baseY=0;fudge=0.15;}
	else 
		{baseX=-1;baseY=0;fudge=-0.15;}
	/*printf("%lf, %lf %lf\n",fabsf(p->posX-baseX),fabsf(p->posY-baseY),PROJECTILE_R);*/

	/*First Base*/
	if(fabsf(p->posX-baseX)<=PROJECTILE_R+0.01&&fabsf(p->posY-baseY)<=PROJECTILE_R){
		return TRUE;
	}
	/*Second Base*/
	if(fabsf(p->posX-baseX+fudge)<=PROJECTILE_R+0.01&&fabsf(p->posY-baseY+0.3)<=PROJECTILE_R){
		return TRUE;
	}
	/*Third Base*/
	if(fabsf(p->posX-baseX+fudge)<=PROJECTILE_R+0.01&&fabsf(p->posY-baseY-0.3)<=PROJECTILE_R){
		return TRUE;
	}
	return FALSE;
}

void freeProjectiles(){
	int i;
	for(i=0;i<50;i++){
		if(projectileArray[i]!=NULL)free(projectileArray[i]);
	}
	free(projectileArray);
}