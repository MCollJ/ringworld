/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/
#include "bmpLoader.h"

/*Retrieves data from .bmp file and stores in an ImageStruct struct,
 then returns a pointer to this struct*/
ImageStructPtr loadBMPimage(char * imageFile){
char header[54];	/*.bmp files have 54 bytes of header information*/
int dataLoc;		/*byte location of beginning of image data*/
int width,height;	/*image width and height*/
int imageSize;		/*image width*imge height*/
char * imageData;	/*actual image data*/

ImageStructPtr image=(ImageStructPtr)malloc(sizeof(ImageStruct));

FILE * file = fopen(imageFile,"rb");
	/*file couldnt be opened*/
	if(file==NULL){
	printf("ERROR: Image file could not be found\n");
	return 0;
	}
	/*Incorrect header size, suggests incorrect file type*/
	if(fread(header,1,54,file)!=54){
		printf("ERROR: Corrupted or incorrect file type\n");
	return 0;
	}
	/*Header does nto specify .bmp file, wrong file type*/
	if ( header[0]!='B' || header[1]!='M' ){
    	printf("ERROR:Not a correct BMP file\n");
   	return 0;
	}
	/*get info data*/
	dataLoc    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);

	/*if info data is missing imageSize calculate it*/
	if (imageSize==0)    imageSize=width*height*3;
	/*if info data is missing data beginning location assume it to be 54 bytes in*/
	if (dataLoc==0)      dataLoc=54;

	/*Copy all info data to struct memory*/
	image->imageSize=imageSize;
	image->height=height;
	image->width=width;

	/*Copy image data to struct memory allocated*/
	image->imageData=(char*)malloc(imageSize);
	fread(image->imageData,1,imageSize,file);

	fclose(file);
	return image;
}
