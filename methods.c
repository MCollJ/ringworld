/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/

#include "assign2.h"
#include "bmpLoader.h"
#include "projectile.h"
#include <time.h>
/*Callback method used for glutIdleFunc()*/

int randNum[500];/*Array to store 500 random values, used for star generation*/

void idle(){
	float time;
	static float lastTime=0.0;
	time=glutGet(GLUT_ELAPSED_TIME);
	time/=1000.0;
	global.difTime=time-lastTime;

	if(global.rotateOn==TRUE){
	global.spin+=global.difTime*global.spinFactor;
	global.spin2+=global.difTime*global.spinFactor*2;
	}
	lastTime=time;

	calculateFPS();
	glutPostRedisplay();
}

/*Calculates time since last call then computes fps*/
void calculateFPS(){
	int difTime,fps,time;
	static int frameCount;
	static float lastTime=0.0;
	frameCount++;
	time = glutGet(GLUT_ELAPSED_TIME);
	difTime = time-lastTime;
	if(difTime>100){
	global.fps=frameCount/(difTime/1000.0f);
	lastTime=time;
	frameCount=0;
	}
}

/*Prints error string if one exists*/
void checkErrors(){
	int err=glGetError(); 
	if(err==GL_NO_ERROR){
	}else{
	printf("%s\n", gluErrorString(err));
	}
}

/*Draws the axes for debugging*/
void drawAxis(){
	glBegin(GL_LINES);
	glPushAttrib(GL_CURRENT_BIT);
	glColor3f(1,0,0);
	glVertex3f(0,0,0);
	glVertex3f(0,1,0);

	glColor3f(0,0,1);
	glVertex3f(0,0,0);
	glVertex3f(1,0,0);

	glColor3f(0,0,1);
	glVertex3f(0,0,0);
	glVertex3f(0,0,1);
	glPopAttrib();
	glEnd();
}

/*Draws the shades and the shades "shadows" by drawing only some of the vertices stored in 
vertices.ringVertices array */
void drawShades(float faces){
	GLuint texture;
	int i,j,count=0;
	
	texture=loadTexture("metalSurfaceSmall.bmp");
	if(normals.shadeNormals!=NULL)free(normals.shadeNormals);
	normals.shadeNormals
		=(float*)calloc(faces*(THREE_D_VERTEX*QUAD_SIZE+1),sizeof(float));

	glPushMatrix();
	for(j=0;j<4;j++){
		glBegin(GL_QUAD_STRIP);
		for(i=2*j*(faces-(faces/4));i<=(2*j+1)*(faces-(faces/4));i+=6){
			/*Populate Shade Normal Array*/
			normals.shadeNormals[count++]=-vertices.ringVertices[i];
			normals.shadeNormals[count++]=-vertices.ringVertices[i+1];
			normals.shadeNormals[count++]=-vertices.ringVertices[i+2];
			normals.shadeNormals[count++]=-vertices.ringVertices[i+3];
			normals.shadeNormals[count++]=-vertices.ringVertices[i+4];
			normals.shadeNormals[count++]=-vertices.ringVertices[i+5];

			/*Since the array is not being built using glDrawArrays, normals
			must be defined explicitly here rather than using glNormalPointer
			and the Normal array*/
			glNormal3f(vertices.ringVertices[i],
				vertices.ringVertices[i+1],vertices.ringVertices[i+2]);
			glTexCoord2f(vertices.ringVertices[i],
				vertices.ringVertices[i+1]);
			glVertex3f(vertices.ringVertices[i],
				vertices.ringVertices[i+1],vertices.ringVertices[i+2]);
			glNormal3f(vertices.ringVertices[i+3],
				vertices.ringVertices[i+4],vertices.ringVertices[i+5]);
			if(i%12==0){glTexCoord2f(vertices.ringVertices[i+3],
				vertices.ringVertices[i+4]);}
			glVertex3f(vertices.ringVertices[i+3],
				vertices.ringVertices[i+4],vertices.ringVertices[i+5]);
		}
	glEnd();
	}
	
	glDeleteTextures(1,&texture);
	/*visualise normals if normalsOn is activated*/
	if(global.normalsOn==TRUE){
		for(i=0;i<=count-3;i+=3){
			drawNormal(normals.shadeNormals[i],
					normals.shadeNormals[i+1],
					normals.shadeNormals[i+2],
					FALSE);
		}
	}
	glPopMatrix();
}

/*Calculate a ring with 'faces' number of faces and place the points in their
respective vertices and normals arrays*/
void genRing(int faces){
	int i,n=faces/2,count=0;
	float x,y;

	/*Part of dynamic memory allocation, frees memory that was previously assigned to hold 
	vertices and normal arrays*/
	if(vertices.ringVertices!=NULL){free(vertices.ringVertices);}
	if(normals.ringNormals!=NULL){free(normals.ringNormals);}
	if(textures.ringTextures!=NULL){free(textures.ringTextures);}
	/*Allocate and clear space in memory large enough for the number of vertices*/
	vertices.ringVertices
		=(float*)calloc(faces*(THREE_D_VERTEX*QUAD_SIZE+1),sizeof(float));
	/*Allocate and clear space in memory large enough for normals*/
	normals.ringNormals
		=(float*)calloc(faces*(THREE_D_VERTEX*QUAD_SIZE+1),sizeof(float));
	textures.ringTextures
		=(float*)calloc(faces*(THREE_D_VERTEX*QUAD_SIZE+1),sizeof(float));

	/*Fill array with calculated vertices and their respective normals*/ 
	for(i=0;i<faces+1;i++){
		x=cos(((180/(float)n)*i)*M_PI/180);
		y=sin(((180/(float)n)*i)*M_PI/180);
		if(i>=n){
		x=-cos(((180/(float)n)*(i-n))*M_PI/180);
		y=-sin(((180/(float)n)*(i-n))*M_PI/180);
		}
		/*VERTEX 1*/
		normals.ringNormals[count]=x;
		textures.ringTextures[count]=x;
		vertices.ringVertices[count++]=x;
		normals.ringNormals[count]=y;
		textures.ringTextures[count]=y;
		vertices.ringVertices[count++]=y;
		normals.ringNormals[count]=-0.1;
		vertices.ringVertices[count++]=-0.1;
		
		/*VERTEX 2*/
		normals.ringNormals[count]=x;
		textures.ringTextures[count]=x;
		vertices.ringVertices[count++]=x;
		normals.ringNormals[count]=y;
		textures.ringTextures[count]=x;
		vertices.ringVertices[count++]=y;
		normals.ringNormals[count]=0.1;
		vertices.ringVertices[count++]=0.1;
	}
}

/*Draw the ring using values stored in the vertices.ringVertices 
array using glDrawArray()*/
void drawRing(int faces){
	int i;
	GLuint texture;
	texture=loadTexture("grassSurface.bmp");

	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3,GL_FLOAT,0,vertices.ringVertices);
	glNormalPointer(GL_FLOAT,0,normals.ringNormals);
	glTexCoordPointer(2,GL_FLOAT,2*sizeof(float),vertices.ringVertices);
	glDrawArrays(GL_QUAD_STRIP,0,faces*2+2);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glDeleteTextures(1,&texture);
	/*Visualise normals if normalsOn is activated*/
	if(global.normalsOn==TRUE){
		for(i=0;i<faces*3*2;i+=3){
			drawNormal(normals.ringNormals[i],
					normals.ringNormals[i+1],
					normals.ringNormals[i+2],
					FALSE);
		}
	}
}

/*Draws a normal from the specified vector co-ordinates with magnitude 3. 
The Sphere Flag is to determine weather the normal vector should be subtracted or 
added when drawn.*/
void drawNormal(float x, float y, float z, int Sphere){
	float mag;
	mag=3*sqrt((x*x)+(y*y)+(z*z));
	glColor3f(YELLOW);
		glPushAttrib(GL_CURRENT_BIT);
		glBegin(GL_LINES);		
		glVertex3f(x,y,z);
		if(Sphere==TRUE){
			glVertex3f(x+(x/mag),y+(y/ mag),z+(z/mag));
		}else{
			glVertex3f(x-(x/mag),y-(y/ mag),z-(z/mag));
		}
		glEnd();
		glPopAttrib();
		glColor3f(1,1,1);
}

/*Draw sphere from sphere array*/
void drawSphere(int slices, int stacks){
	int i;
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,vertices.sphereVertices);
	glNormalPointer(GL_FLOAT,0,normals.sphereNormals);
	glDrawArrays(GL_TRIANGLES,0,slices*stacks*6);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	
	if(global.normalsOn==TRUE){
		for(i=0; i<slices*stacks*16;i+=3){
			drawNormal(normals.sphereNormals[i],
					normals.sphereNormals[i+1],
					normals.sphereNormals[i+2],
					TRUE);
		}
	}
}

/*Most of this sphere algorithm was taken from the week 5 lecture on procedural generation, 
written by Geoff Leach.*/ 
void genSphere(int slices, int stacks,float r){
	float theta, phi;
	float x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
	float step_theta = 2.0 * M_PI / slices, step_phi = M_PI / stacks;
	float * sphereVertexPtr;
	float * sphereNormalPtr;
	int j,i;

	if(vertices.sphereVertices!=NULL)free(vertices.sphereVertices);
	if(normals.sphereNormals!=NULL)free(normals.sphereNormals);

	vertices.sphereVertices=(float*)calloc(18*slices*stacks*2,sizeof(float));
	normals.sphereNormals=(float*)calloc(18*slices*stacks*2,sizeof(float));
	sphereVertexPtr=vertices.sphereVertices;
	sphereNormalPtr=normals.sphereNormals;

	glPushMatrix();
	for (j = 0; j <= stacks; j++) {
	phi = j / (float)stacks * M_PI;
		for (i = 0; i < slices; i++) {
		theta = i / (float)slices * 2.0 * M_PI;  
		x1 = r * sinf(phi) * cosf(theta);
		y1 = r * sinf(phi) * sinf(theta);
		z1 = r * cosf(phi);
		x2 = r * sinf(phi) * cosf(theta + step_theta);
		y2 = r * sinf(phi) * sinf(theta + step_theta);
		z2 = r * cosf(phi);
		x3 = r * sinf(phi + step_phi) * cosf(theta + step_theta);
		y3 = r * sinf(phi + step_phi) * sinf(theta + step_theta);
		z3 = r * cosf(phi + step_phi);
		x4 = r * sinf(phi + step_phi) * cosf(theta);
		y4 = r * sinf(phi + step_phi) * sinf(theta);
		z4 = r * cosf(phi + step_phi);

		*sphereVertexPtr++=x1;
		*sphereVertexPtr++=y1;
		*sphereVertexPtr++=z1;
		*sphereVertexPtr++=x2;
		*sphereVertexPtr++=y2;
		*sphereVertexPtr++=z2;
		*sphereVertexPtr++=x3;
		*sphereVertexPtr++=y3;
		*sphereVertexPtr++=z3;
		*sphereVertexPtr++=x1;
		*sphereVertexPtr++=y1;
		*sphereVertexPtr++=z1;
		*sphereVertexPtr++=x3;
		*sphereVertexPtr++=y3;
		*sphereVertexPtr++=z3;
		*sphereVertexPtr++=x4;
		*sphereVertexPtr++=y4;
		*sphereVertexPtr++=z4;

		*sphereNormalPtr++=-x1;
		*sphereNormalPtr++=-y1;
		*sphereNormalPtr++=-z1;
		*sphereNormalPtr++=-x2;
		*sphereNormalPtr++=-y2;
		*sphereNormalPtr++=-z2;
		*sphereNormalPtr++=-x3;
		*sphereNormalPtr++=-y3;
		*sphereNormalPtr++=-z3;
		*sphereNormalPtr++=-x1;
		*sphereNormalPtr++=-y1;
		*sphereNormalPtr++=-z1;
		*sphereNormalPtr++=-x3;
		*sphereNormalPtr++=-y3;
		*sphereNormalPtr++=-z3;
		*sphereNormalPtr++=-x4;
		*sphereNormalPtr++=-y4;
		*sphereNormalPtr++=-z4;
		}
	}
	glPopMatrix();
}

/*Draw specified text at screen location x,y. x,y<100*/
void drawText(char* text,int x,int y){
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glColor3f(1,1,1);
	gluOrtho2D(0,100,0,100);
	glColor3f(1,1,1);
	glRasterPos2f(x,y);
	glColor3f(1,1,1);
	glutBitmapString(GLUT_BITMAP_9_BY_15, (unsigned char*)text);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

/*Draws the onscreen display*/
void drawGui(){
	char buffer[64];
	GLint viewport[4];
	glDisable(GL_LIGHTING);
	glViewport(0,0,global.windowWidth,global.windowHeight);
	glPushAttrib(GL_CURRENT_BIT);
	glGetIntegerv(GL_VIEWPORT,viewport);
	sprintf(buffer,"%s:%d","Frame Rate:",global.fps);
	drawText(buffer,5,12);
	sprintf(buffer,"%s:%f","Frame Time:",(float)1/global.fps);
	drawText(buffer,5,9);
	sprintf(buffer,"Wireframe: %s",global.polygonMode ? "FALSE" : "TRUE");
	drawText(buffer,5,6);
	sprintf(buffer,"Normals: %s", global.normalsOn ? "TRUE" : "FALSE");
	drawText(buffer,5,3);
	glPopAttrib();
	glEnable(GL_LIGHTING);
}

/*Draw the bases one each side*/
void drawBases(int faces){
	int tesselation,i;
	float scale=1.0/2.0;
	tesselation= faces*faces/2*3;	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	
	for(i=-3;i<3;i++){
	/*Apply the transformations*/
		/*LEFT BASES*/
		if(i<0){
		glRotatef(-90,0,1,0);
		glRotatef(40+(i*20),1,0,0);
		glColor3f(RED);
		}
		/*RIGHT BASES*/
		else{
		glRotatef(90,0,1,0);
		glRotatef(-20+(i*20),1,0,0);
		glColor3f(GREEN);
		}
	glTranslatef(0,0,-0.99);
	glScalef(scale,scale,scale);
	
	/*Draw hemisphere*/
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);	
	glVertexPointer(3,GL_FLOAT,0,vertices.sphereVertices);
	glNormalPointer(GL_FLOAT,0,normals.sphereNormals);
	glDrawArrays(GL_TRIANGLES,0,tesselation/2);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	/*Reverse the transformations*/
	glScalef(1/scale,1/scale,1/scale);
	glTranslatef(0,0,0.99);
		if(i<0){
		glRotatef(-40-(i*20),1,0,0);
		glRotatef(90,0,1,0);
		}else{
		glRotatef(-(-20+(i*20)),1,0,0);	
		glRotatef(-90,0,1,0);
		}
	}
	glColor3f(1,1,1);
	glPopMatrix();
}

/*Load a .bmp image file, specified by location fileString, as a texture in graphics memory*/
GLuint loadTexture(char * fileString){
	GLuint textureID;
	ImageStructPtr image;
	/*Load the image data from file at fileString*/
	image=loadBMPimage(fileString);
	
	/*Bind textureId to image data*/
	glGenTextures(1,&textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  	
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->width, image->height,
                       GL_BGR_EXT, GL_UNSIGNED_BYTE, image->imageData );
	/*MUST be GL_BGR_EXT, since .bmp files are encoded in Blue,Green,Red*/

	/*delete image data from local memory since it is now stored in graphics memory*/
	free(image->imageData);
	free(image);
	return textureID;
}

/*Draws the starfield*/
void drawStarBox(){
	int i,j,slices=32,stacks=32;
	glRotatef(-global.spin,0,0,1);
	glPushMatrix();
	/*scale to a suitable size*/
	glScalef(20,20,20);
	/*Draw all points*/
	glBegin(GL_POINTS);
	for(i=0;i<slices*stacks*4;i++){
		for(j=0;j<500;j++){
			if(randNum[j]==i)
			{
				glVertex3f(vertices.sphereVertices[i],
					vertices.sphereVertices[i+1],
					vertices.sphereVertices[i+2]);
			}
		}
	}
	glEnd();
	glPopMatrix();
	glRotatef(global.spin,0,0,1);
}

/*Generate the random values for drawStarBox()*/
void genStarBox(){
	int i;
	srand(time(NULL));
	for(i=0;i<500;i++){
		randNum[i]=rand()%(32*32*6);
	}
}

/*Free all allocated memory*/
void freeMemory(){
	int i;
	freeProjectiles();
	if(vertices.shadeVertices!=NULL)free(vertices.shadeVertices);
	if(vertices.ringVertices!=NULL)free(vertices.ringVertices);
	if(vertices.sphereVertices!=NULL)free(vertices.sphereVertices);

	if(normals.sphereNormals!=NULL)free(normals.sphereNormals);
	if(normals.ringNormals!=NULL)free(normals.ringNormals);
	if(normals.shadeNormals!=NULL)free(normals.shadeNormals);
}