/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/
#include "projectile.h"
#include "assign2.h"


float lastX=0, lastY=0; /*Reset the last recorded mouse x and y*/
	

/*Default lighting values*/
float lightPosition[] = {0.1, 0.1, 0.1, 0.1},	
		whiteDiffuseL[]={0.5,0.5,0.5},
		blackAmbientL[] = {0.6, 0.6, 0.6};
		GLfloat whiteSpecularL=1;

/*Main display method*/
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	setView();
	drawGui();
	drawScene();
	drawAllProjectiles();
	checkErrors();
	glutSwapBuffers();
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(600,600);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

	glutCreateWindow("Assignment 2");
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	/*Initiate global variables*/
	global.faces = NUM_START_FACES;
	global.spinFactor=3;
	global.polygonMode=LINED;
	global.rotateOn=TRUE;
	global.fov=60;
	global.aspect=1;
	global.fps=0;
	global.viewMode=1;
	global.windowWidth=600;
	global.windowHeight=600;
	global.spin=0;	/*Initialise value for rotation animation*/
	global.spin2=0;
	global.redGunAngle=0;	/*Initialise value for projectile firing angle*/
	global.greenGunAngle=0;
	initialiseProjectileArray();

	/*Generate ring and sphere with a beginning value of faces (NUM_START_FACES)*/
	genRing(global.faces);
	genSphere(global.faces/2,global.faces/2,0.2);
	genStarBox();
	
	/*Initiate lighting*/
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightf(GL_LIGHT0, GL_SPECULAR, whiteSpecularL);
	glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientL);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseL);

	/*Enable lighting,colours to materials and normalise all normals*/
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	/*Anti z-fighting tool*/
	glEnable(GL_DEPTH_TEST);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutKeyboardFunc(key);
	glutSpecialFunc(specialKey);
	glutMouseFunc(mousePressed);
	glutMotionFunc(mouse);
	glutMainLoop();
}

/*Build viewports depending on window mode and dimensions and do some initial translations*/
void setView(){
	/*Initial translations and rotations for the scene*/
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*If NOT splitscreen*/	
	if(global.viewMode==1)
	{	
	glTranslatef(0,0,-3);
	glViewport(0, 0, global.windowWidth, global.windowHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(global.fov,global.windowWidth/global.windowHeight,0.1,1000);
	}
	/*If IS splitscreen*/	
	else{
	glTranslatef(0,0,-0.9);
	glRotatef(-90,0,1,0);
	glRotatef(-90,1,0,0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(global.fov-20,global.windowWidth/global.windowHeight/0.5,0.1,1000);
	}
}

/*Simple reshape method*/
void reshape(int width, int height){
	global.windowWidth=width;
	global.windowHeight=height;
}

/*draws all continuous elements in the scene*/
void drawScene(){
	int i;
	/*Set up each viewport, 2 if global.viewMode==2 else only 1*/
	for(i=0;i<global.viewMode;i++)
	{
		/*Check if global.viewMode==2 and split screen is on then flip one viewport*/
		if(i==1){
			glViewport(0, global.windowHeight/2, global.windowWidth,global.windowHeight/2);
			glRotatef(-180,0,1,0);
		}else{
			glViewport(0, 0, global.windowWidth, global.windowHeight/2);
		}
		/*If split screen is off set view as normal*/
		if(global.viewMode==1){
			glViewport(0, 0, global.windowWidth, global.windowHeight);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(global.fov,global.windowWidth/global.windowHeight,0.1,1000);
		}
	
	/*Rotation using mouse movement*/
	glMatrixMode(GL_MODELVIEW);
	glRotatef(global.rotateX,1.0,0.0,0.0); 
   	glRotatef(global.rotateY,0.0,1.0,0.0);
	
	/*Animation*/
	glRotatef(global.spin,0,0,1);
	
	/*Draw the Sun*/
	glPushMatrix();
	glScalef(0.4, 0.4, 0.4);
	glColor3f(YELLOW);
	drawSphere(global.faces/2,global.faces/2);
	glColor3f(1,1,1);
	glPopMatrix();	

	/*Draw the "shadows" of the sails and rotate 
	at twice the speed of the ring*/
	glRotatef(global.spin2,0,0,1);
	glPushMatrix();
	glScalef(0.995,0.995,0.995);
	glColor3f(0,0,0);
	drawShades(global.faces);
	glColor3f(1,1,1);
	glPopMatrix();

   	/*Draw the actual shade sails (by scaling the "shadows")*/
	glPushMatrix();
	glScalef(0.4,0.4,0.4);
	drawShades(global.faces);
	glPopMatrix();
	
	/*Reset the rotation to the original rotation speed*/
	glRotatef(-global.spin2,0,0,1);
	if(global.viewMode==2){
		glRotatef(-global.spin,0,0,1);
		drawAllProjectiles();
	}

   	/*Draw the ring*/
	drawBases(global.faces);
   	drawRing(global.faces);	
	drawStarBox();

	/*Dynamic memory allocation for normal array*/
	}

}
