/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define M_PI (4.f * atan(1.f))
#include <Windows.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

/*CONSTANTS*/
#define QUAD_SIZE 4
#define TRIANGLE_SIZE 3
#define THREE_D_VERTEX 3
#define YELLOW 255,255,0.0
#define RED 255,0,0
#define GREEN 0,204,0
#define FALSE 0
#define TRUE 1
#define NUM_START_FACES 32
#define FILLED 1
#define LINED 0
#define MOUSE_RIGHT 1
#define MOUSE_LEFT 0
#define PROJECTILE_R 0.05
#define G_CONSTANT 2
#define MAX_PROJECTILES 50
#define SPACE_KEY 32
#define LEFT_ARROW 37
#define RIGHT_ARROW 39
#define ESC_KEY 27

/*METHOD DECLARATIONS*/
void drawAxis();
void freeMemory();
void key(unsigned char, int, int);
void specialKey(int key, int x, int y);
void mouse(int,int);
void mousePressed(int,int,int,int);
void drawSphere(int, int);
void drawShades(float);
void drawRing(int);
void drawNormal(float,float,float,int);
void drawText(char*,int,int);
void checkErrors();
void idle();
void genRing(int);
void genSphere(int slices, int stacks,float r);
void calculateFPS();
void drawGui();
void drawScene();
void setView();
void reshape(int,int);
void drawBases(int);
void createProjectile();
void genStarBox();
void drawStarBox();
GLuint loadTexture(char *);

extern float lastX,lastY;

/*Struct to contain vertex arrays*/
typedef struct{
	float* ringVertices;
	float* sphereVertices;
	float* shadeVertices;
}Vertices;

/*Struct to contain normal arrays*/
typedef struct{
	float* ringNormals;
	float* sphereNormals;
	float* shadeNormals;
}Normals;

typedef struct{
	float* ringTextures;
	float* sphereTextures;
	float* shadeTextures;
}Textures;

/*Struct to contain global variables*/
typedef struct{
float rotateX,rotateY;	
float xAmount,yAmount;
float spinFactor,	/*Used to set rotation speed*/
	spin,
	spin2,	
	fov,		/*Field of view for perspective view*/
	windowWidth,
	windowHeight,
	greenGunAngle,
	redGunAngle,
	difTime;		
int mouseButton, 	/*Defines whether right or left mouse button is clicked*/
	faces,		/*Number of faces for shapes*/
	normalsOn, /*Switch to visualise normals*/
	polygonMode,	/*Wireframe or solid switch*/
	rotateOn,		/*Animation switch*/
	fps,
	aspect,
	viewMode,
	redProjectile;
}Global;

Global global;
Normals normals;
Vertices vertices;
Textures textures;
