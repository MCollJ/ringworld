/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/

README

Known Bugs:

-OpenGL,GLUT and Nvidia libraries all seem to have memory leaks. 
	Valgrind Report:

	LEAK SUMMARY:
	definitely lost: 262,392 bytes in 5 blocks
	indirectly lost: 1,122 bytes in 2 blocks
	possibly lost: 0 bytes in 0 blocks
	still reachable: 92,021 bytes in 492 blocks
	suppressed: 0 bytes in 0 blocks

	Each leak references nvidia/libGL or usr/lib64/libglut files.

	My code appears leak free.

Missing Functionality:

/*ADVANCED FEATURES*/
-No Audio 
-Split shading
-Transperancy
-Rocket model