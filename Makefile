all:assign2

assign2: assign2.c events.c methods.c bmpLoader.c projectile.c
	gcc -o assign2 -g -lGL -lGLU -lglut -lm assign2.c events.c methods.c bmpLoader.c projectile.c
	
assign2.o: assign2.c
	gcc -g -lGL -lGLU -lglut -lm -c assign2.c
	
events.o: events.c
	gcc -g -lGL -lGLU -lglut -lm -c events.c
	
options.o: options.c
	gcc -g -lGL -lGLU -lglut -lm -c options.c

bmpLoader.o: bmpLoader.c
	gcc -g -lGL -lGLU -lglut -lm -c bmpLoader.c
	
projectile.o: projectile.c
	gcc -g -lGL -lGLU -lglut -lm -c projectile.c

clean:
	rm -rf *o assign2 core

archive:
	zip $(USER)-a2.zip assign2.c assign2.h bmpLoader.c bmpLoader.h projectile.c projectile.h methods.c events.c \
	readme.txt grassSurface.bmp metalSurfaceSmall.bmp Makefile
