/****************************************************************************
* COSC1186/1187 - Interactive 3D Graphics and Animation
* Semester 1 2013 Assignment #2 
* Full Name        : Michael Collins
* Student Number   : 3370092
* Yallara Username : s3370092
* Course Code      : COSC1186
* Program Code     : BP096
***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*Struct to hold data retrieved from .bmp file*/
typedef struct image{
	int width;
	int height;
	int imageSize;
	char * imageData;
}ImageStruct;

typedef struct image* ImageStructPtr;

ImageStructPtr loadBMPimage(char *);
